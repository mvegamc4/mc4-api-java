package baseapitest;

import com.google.common.io.Resources;
import org.apache.commons.mail.*;
import org.junit.Before;
import org.junit.Test;

import javax.mail.Authenticator;
import java.net.MalformedURLException;
import java.net.URL;

/**
 * @author Miguel Vega
 * @version $Id: SendEmailTest.java 0, 2013-01-30 6:39 PM mvega $
 */
public class SendEmailTest {
    <T extends Email> T configure(T email) throws EmailException {
        email.setHostName("smtp.googlemail.com");
        email.setSmtpPort(465);
        email.setAuthenticator(new DefaultAuthenticator("miguelvega.name@gmail.com", "Mavp8584"));
        email.setSSL(true);
        email.setFrom("miguelvega.name@gmail.com");
        return email;
    }

    @Test
    public void testSendSimpleEmail() throws EmailException {
        Email email = new SimpleEmail();
        email = configure(email);
        email.setSubject("TestMail");
        email.setMsg("This is a test mail ... :-)");
        email.addTo("mikevegap@gmail.com");
        email.send();
    }

    @Test
    public void testSendEmailWithAttachement() throws EmailException {
        // Create the attachment
        EmailAttachment attachment = new EmailAttachment();
        //attachment.setPath("mypictures/john.jpg");
        attachment.setURL(Resources.getResource("logo_sun.gif"));
        attachment.setDisposition(EmailAttachment.ATTACHMENT);
        attachment.setDescription("Picture of John");
        attachment.setName("John");

        // Create the email message
        MultiPartEmail email = new MultiPartEmail();
        email = configure(email);
        email.addTo("mikevegap@gmail.com", "Miguel Vega");
        email.setSubject("The picture");
        email.setMsg("Here is the picture you wanted");

        // add the attachment
        email.attach(attachment);

        // send the email
        email.send();
    }

    @Test
    public void testSendHtmlEmail() throws EmailException, MalformedURLException {
        // Create the email message
        HtmlEmail email = new HtmlEmail();
        email = configure(email);
        email.addTo("mikevegap@gmail.com", "Mike");
        email.setSubject("Test email with inline image");

        // embed the image and get the content id
        URL url = new URL("http://www.apache.org/images/asf_logo_wide.gif");
        String cid = email.embed(url, "Apache logo");

        // set the html message
        email.setHtmlMsg("<html>The apache logo - <a href=\"http://commons.apache.org/email/\"><img src=\"cid:"+cid+"\"></a></html>");

        // set the alternative message
        email.setTextMsg("Your email client does not support HTML messages");

        // send the email
        email.send();
    }
}
