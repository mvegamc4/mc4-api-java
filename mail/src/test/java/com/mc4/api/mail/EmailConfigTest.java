package com.mc4.api.mail;

import org.apache.commons.mail.DefaultAuthenticator;
import org.apache.commons.mail.EmailException;
import org.apache.commons.mail.SimpleEmail;
import org.junit.Test;

/**
 * @author Miguel Vega
 * @version $Id: EmailConfigTest.java 0, 2013-01-30 7:28 PM mvega $
 */
public class EmailConfigTest {
    @Test
    public void testSendEmail() throws EmailException {
         EmailConfig emailConfig = new EmailConfig().
                 setAuthenticator(new DefaultAuthenticator("miguelvega.name@gmail.com", "Mavp8584")).
                 setFrom("miguelvega.name@gmail.com").setHostName("smtp.googlemail.com").setSSL(true).setSmtpPort(465);
        SimpleEmail simpleEmail = emailConfig.newSimpleEmail();

        simpleEmail.setSubject("hello world!");
        simpleEmail.setMsg("This is a simple plain text email");
        simpleEmail.addTo("mikevegap@gmail.com", "mike");

        simpleEmail.send();
    }
}
