package com.mc4.api.mail;

import com.google.common.collect.Maps;
import org.apache.commons.mail.*;

import javax.mail.Authenticator;
import java.util.Map;

/**
 * @author Miguel Vega
 * @version $Id: EmailConfig.java 0, 2013-01-30 7:12 PM mvega $
 */
public class EmailConfig {
    private Map<String, Object> config = Maps.newHashMap();

    public SimpleEmail newSimpleEmail() throws EmailException {
        return newEmail(SimpleEmail.class);
    }

    public HtmlEmail newHtmlEmail() throws EmailException {
        return newEmail(HtmlEmail.class);
    }

    public MultiPartEmail newMultiPartEmail() throws EmailException {
        return newEmail(MultiPartEmail.class);
    }

    /**
     * Creates an instance of given {@link Email} subclass using empty constructor
     * @param v
     * @param <V>
     * @return
     */
    public <V extends Email> V newEmail(Class<V> v) throws EmailException {
        try {
            V email = v.newInstance();

            //configure
            email.setHostName((String)config.get("hn"));
            email.setSmtpPort((Integer)config.get("smtp"));
            email.setAuthenticator((Authenticator) config.get("auth"));
            email.setSSL((Boolean) config.get("ssl"));
            email.setFrom((String) config.get("from"));

            return email;
        } catch (InstantiationException e) {
            throw new IllegalStateException("Unexpected error", e);
        } catch (IllegalAccessException e) {
            throw new IllegalStateException("Unexpected error", e);
        }catch(NullPointerException x){
            throw new IllegalStateException("A parameter was not defined.");
        }
    }

    public EmailConfig setHostName(String hostName){
        config.put("hn", hostName);
        return this;
    }

    public EmailConfig setAuthenticator(Authenticator authenticator){
        config.put("auth", authenticator);
        return this;
    }

    public EmailConfig setFrom(String from){
        config.put("from", from);
        return this;
    }

    public EmailConfig setSSL(boolean from){
        config.put("ssl", from);
        return this;
    }

    public EmailConfig setSmtpPort(int smtpPort){
        config.put("smtp", smtpPort);
        return this;
    }
}
