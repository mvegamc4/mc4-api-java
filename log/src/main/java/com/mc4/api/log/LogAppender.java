package com.mc4.api.log;

import org.apache.log4j.Level;
import org.apache.log4j.AppenderSkeleton;

/**
 * Will give the capabilities to treat the logger messages at implementations.
 * @author Miguel Vega
 * @version $Id: LogAppender.java 0, 2013-01-11 7:37 PM mvega $
 */
public abstract class LogAppender<T>{
    private String name;
    public LogAppender(String name){
        assert name!=null;

        this.name = name;
    }
    /**
     * Should implement this method to perform actual logging. See also {@AppenderSkeleton#doAppend} method.
     * @param level
     * @param message
     */
    public abstract void log(Level level, T message);

    /**
     * Release any resources allocated within the appender such as file handles, network connections, etc.
     * </br>
     * It is a programming error to append to a closed appender.
     */
    public void close(){
        //dummy, ready to be override
    }

    public String getName() {
        return name;
    }
}