package com.mc4.api.log;

import java.lang.annotation.*;

/**
 * @author Miguel Vega
 * @version $Id: NamedLogAppender.java 0, 2013-01-14 23:56 mvega $
 */
@Retention(RetentionPolicy.RUNTIME)
//@Target(ElementType.TYPE)
@Documented
public @interface NamedLogAppender {
    public String name();
}
