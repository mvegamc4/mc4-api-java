package com.mc4.api.log;

import com.google.common.base.Function;
import com.google.common.base.Optional;
import com.google.common.cache.Cache;
import com.google.common.cache.CacheBuilder;
import com.google.common.collect.Maps;
import org.apache.log4j.*;
import org.apache.log4j.spi.LoggingEvent;

import java.util.Map;

import static com.google.common.collect.Iterables.transform;
import static org.apache.log4j.Logger.getRootLogger;

/**
 * This class allows to configure the log4j logger without external log4j.properties file is needed.
 * Also allows to manage all the logger behaviors such as change level of loggers.
 *
 * @author Miguel Vega
 * @version $Id: LoggerSettings.java 0, 2013-01-11 6:50 PM mvega $
 */
public class LoggerSettings {
    //use the following structure to store the registered appenders
    private static Map<String, LogAppender> logBindings = Maps.newHashMap();
    //when a logger is created, must check if the given object is annotated with {@ NamedLogAppender}, if so
    // then store the object in the cache
    private static Cache<String, String> cache;

    static final LoggerSettingsMasterAppenderSkeleton appender = new LoggerSettingsMasterAppenderSkeleton();

    static {
        Logger rootLogger = getRootLogger();
        //by default add a console logger if no log4j.properties file is bound with any appender
        if (!rootLogger.getAllAppenders().hasMoreElements())
            rootLogger.addAppender(new ConsoleAppender(new PatternLayout("%r [%t] %-5p %c %x - %m%n")));

        rootLogger.addAppender(appender);

        //initialize the cache
        cache = CacheBuilder.newBuilder().build();
    }

    /**
     * Register an appender to reuse it in the future
     *
     * @param appender
     */
    public static void registerLogAppender(LogAppender appender) {
        logBindings.put(appender.getName(), appender);
        Logger.getLogger(appender.getName());
    }

    public static void setLogLevel(String name, Level level) {
        Logger logger = getRootLogger().getLoggerRepository().getLogger(name);
        logger.setLevel(level);
    }

    /**
     * Binds a class annotated with @NamedLogAppender
     *
     * @param owner
     * @param <T>
     * @return
     */
    public static <T extends LogAppender> Logger getInstance(Object owner) {
        assert owner != null && appender != null;

        //retrieve the name of the LogAppender registration name if exists
        String name = Optional.fromNullable(getNamedLogAppenderName(owner)).or("");

        Logger logger = Logger.getLogger(getLoggerName(owner));

        cache.put(logger.getName(), name);

        return logger;
    }

    static String getLoggerName(Object owner){
        assert owner!=null;

        if(Class.class.isAssignableFrom(owner.getClass())){
            return ((Class)owner).getName();
        }
        return owner.toString();
    }

    static String getNamedLogAppenderName(Object o) {

        if (Class.class.isAssignableFrom(o.getClass())) {
            NamedLogAppender nl = (NamedLogAppender) ((Class) o).getAnnotation(NamedLogAppender.class);
            //return nl != null ? nl.name() : ((Class) o).getName();
            return nl != null ? nl.name() : null;
        }

        NamedLogAppender nl = o.getClass().getAnnotation(NamedLogAppender.class);
        //return nl != null ? nl.name() : o.getClass().getName();
        return nl != null ? nl.name() : null;
    }

    static class LoggerSettingsMasterAppenderSkeleton extends AppenderSkeleton {
        @Override
        protected void append(LoggingEvent event) {
            Level level = event.getLevel();

            String annotated = Optional.fromNullable(cache.getIfPresent(event.getLoggerName())).or(event.getLoggerName());

            Object message = event.getMessage();
            Optional.<LogAppender>fromNullable(logBindings.get(annotated)).or(new LogAppender("") {
                @Override
                public void log(Level level, Object message) {
                    //dummy
                }
            }).log(level, message);
        }

        @Override
        public void close() {
            Iterable<LogAppender> transform = transform(logBindings.entrySet(), new Function<Map.Entry<String, LogAppender>, LogAppender>() {
                @Override
                public LogAppender apply(Map.Entry<String, LogAppender> input) {
                    LogAppender value = input.getValue();
                    value.close();
                    return value;
                }
            });
        }

        @Override
        public boolean requiresLayout() {
            return true;
        }
    }
}