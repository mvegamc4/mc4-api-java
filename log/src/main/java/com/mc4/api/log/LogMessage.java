package com.mc4.api.log;

import java.io.Serializable;
import java.util.Map;

/**
 * @author Miguel Vega
 * @version $Id: LogMessage.java 0, 2013-01-28 11:11 PM mvega $
 */
public class LogMessage {
    private String text;
    private Map<String, ? extends Serializable> params;

    LogMessage(String text, Map params) {
        this.text = text;
        this.params = params;
    }

    public static LogMessage of(String text, Map<String, ? extends Serializable> params){
        return new LogMessage(text, params);
    }

    @Override
    public String toString() {
        return text;
    }

    public Map getParams() {
        return params;
    }
}
