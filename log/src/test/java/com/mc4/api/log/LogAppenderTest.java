package com.mc4.api.log;

import com.google.common.base.Predicates;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableMap;
import com.google.inject.AbstractModule;
import com.google.inject.Inject;
import com.google.inject.Injector;
import com.mc4.api.log.LogAppender;
import com.mc4.api.log.NamedLogAppender;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.junit.Before;
import org.junit.Test;

import java.util.Date;

import static com.google.inject.Guice.createInjector;
import static com.mc4.api.log.LoggerSettings.getInstance;
import static com.mc4.api.log.LoggerSettings.registerLogAppender;
import static junit.framework.Assert.assertTrue;

/**
 * @author Miguel Vega
 * @version $Id: LogAppenderTest.java 0, 2013-01-15 00:31 mvega $
 */
public class LogAppenderTest {

    Logger LOGGER = getInstance(LogAppender.class);

    @NamedLogAppender(name = "one")
    class StaticObj{
        Logger logger = getInstance(StaticObj.class);
        public void doIt(){
            logger.info("from static logger");
        }
    }

    @NamedLogAppender(name = "one")
    class NonStaticObj{
        Logger logger;
        NonStaticObj(){
            logger = getInstance(this);
        }
        NonStaticObj(String logName){
            logger = Logger.getLogger(logName);
        }
        public void doIt(){
            logger.info("from NON static logger");
        }
    }

    public static class InjectedObj{

        Logger logger;
        @Inject
        InjectedObj(Logger logger){
            this.logger = logger;
        }
        public void doIt(){
            logger.info("from injected logger");
        }
    }

    @Before
    public void init(){
        //register all the LogAppenders
        registerLogAppender(new LogAppender("one") {
            @Override
            public void log(Level level, Object message) {
                assertTrue(Predicates.in(ImmutableList.<String>of("from static logger", "from NON static logger")).
                        apply(message.toString()));
                LOGGER.info("Appender named 'ONE' received: " + message + ", @ level:" + level.toString());
            }

            @Override
            public void close() {
                LOGGER.info("Release some resources. Not mandatory.");
            }
        });

        registerLogAppender(new LogAppender("two") {
            @Override
            public void log(Level level, Object message) {
                assertTrue(Predicates.equalTo("from injected logger").
                        apply(message.toString()));
                LOGGER.info("Appender named 'TWO' received: " + message + ", @ level:" + level.toString());
            }
        });

        registerLogAppender(new LogAppender<LogMessage>("Three") {
            @Override
            public void log(Level level, LogMessage message) {
                LOGGER.info("Appender named 'THREE' received: " + message + ", @ level:" + level.toString()+
                        ", PARAMETERS: "+message.getParams());
            }
            //new NonStaticObj("not registered logger").doIt();
        });
    }

    @NamedLogAppender(name = "Three")
    void doItOnMethodAnnotated(){
        Logger logger = getInstance(this);
        logger.info(LogMessage.of("Hello from an annotated method!!!", ImmutableMap.of("datetime", new Date().toString())));
    }

    @Test
    public void testLoggerSettingsInfo(){
        //define the objects to log the events
        Injector injector = createInjector(new AbstractModule() {
            @Override
            protected void configure() {
                //can create & inject a logger which contains the name of the registered log appender, and i works too
                bind(Logger.class).toInstance(Logger.getLogger("two"));
            }
        });

        new StaticObj().doIt();
        new NonStaticObj().doIt();
        new NonStaticObj("not registered logger").doIt();
        injector.getInstance(InjectedObj.class).doIt();

        doItOnMethodAnnotated();
    }
}