package com.mc4.api.log;

import junit.framework.Assert;
import org.apache.log4j.*;
import org.apache.log4j.Logger;
import org.junit.Before;
import org.junit.Test;

import java.util.Enumeration;

import static junit.framework.Assert.*;
import static org.apache.log4j.Level.*;

/**
 * @author Miguel Vega
 * @version $Id: Log4jTest.java 0, 2013-01-11 5:43 PM mvega $
 */
public class Log4jTest {
    @Before
    public void init(){
        org.apache.log4j.Logger root = org.apache.log4j.Logger.getRootLogger();
        root.setLevel(INFO);
        //root.addAppender(new ConsoleAppender(new PatternLayout("%d{ISO8601} [%p] %c - %m%n")));
        //root.addAppender(new ConsoleAppender(new PatternLayout("%r [%t] %-5p %c %x - %m%n")));
        root.addAppender(new ConsoleAppender(new PatternLayout("%r [%t] %-5p %c %x - %m%n")));
    }

    /**
     * Test about the log4j levels
     */
    @Test
    public void testLogLevels(){
        org.apache.log4j.Logger logger = org.apache.log4j.Logger.getLogger(getClass());

        logger.trace("@Trace");
        logger.debug("@Debug");
        logger.info("@Info");
        logger.warn("@Warn");
        logger.error("@Error");
        logger.fatal("@Fatal");
    }

    @Test
    public void testMultipleLoggers(){
        org.apache.log4j.Logger A = org.apache.log4j.Logger.getLogger("A");
        org.apache.log4j.Logger B = org.apache.log4j.Logger.getLogger("B");

        A.setLevel(ERROR);
        A.trace("by A");
        A.debug("by A");
        A.info("by A");
        A.warn("by A");
        A.error("by A");
        A.fatal("by A");

        System.out.println();

        B.setLevel(TRACE);
        B.trace("debug level B");
        B.debug("debug level B");
        B.info("debug level B");
        B.warn("debug level B");
        B.error("debug level B");
        B.fatal("debug level B");
        assertTrue(B.isTraceEnabled());

        System.out.println();

        B.setLevel(FATAL);
        B.trace("fatal level B");
        B.debug("fatal level B");
        B.info("fatal level B");
        B.warn("fatal level B");
        B.error("fatal level B");
        B.fatal("fatal level B");
        assertFalse(B.isTraceEnabled());
    }

    @Test
    public void testRetrieveAllLoggers(){
        org.apache.log4j.Logger.getLogger("A");
        org.apache.log4j.Logger.getLogger("B");
        org.apache.log4j.Logger.getLogger("C");

        assertNotNull(Logger.getRootLogger().getLoggerRepository().getLogger("A"));
        assertNotNull(Logger.getRootLogger().getLoggerRepository().getLogger("B"));
        assertNotNull(Logger.getRootLogger().getLoggerRepository().getLogger("C"));
        //assertNull(Logger.getRootLogger().getLoggerRepository().getLogger("D"));

        Enumeration<org.apache.log4j.Logger> loggers = Logger.getRootLogger().getLoggerRepository().getCurrentLoggers();
        while (loggers.hasMoreElements()){
            Logger logger = loggers.nextElement();

            System.out.println("Logger found. "+logger.getName());
        }
    }
}
