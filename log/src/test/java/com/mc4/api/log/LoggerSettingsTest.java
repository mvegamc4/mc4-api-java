package com.mc4.api.log;

import com.google.common.base.Predicates;
import com.google.common.collect.ImmutableList;
import com.mc4.api.log.LogAppender;
import com.mc4.api.log.LoggerSettings;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.junit.Test;

import static junit.framework.Assert.assertEquals;
import static junit.framework.Assert.assertTrue;

/**
 * @author Miguel Vega
 * @version $Id: LoggerSettingsTest.java 0, 2013-01-15 01:58 mvega $
 */
public class LoggerSettingsTest {
    @Test
    public void testRegisterLogAppender() throws Exception {
        String name = "mvp";
        LoggerSettings.registerLogAppender(new LogAppender<String>(name) {
            @Override
            public void log(Level level, String message) {
                assertEquals("Hello World!", message);
            }
        });
        LoggerSettings.getInstance(name).info("Hello World!");
    }

    @Test
    public void testSetLogLevel() throws Exception {
        String name = "mvp";
        LoggerSettings.registerLogAppender(new LogAppender<String>(name) {
            @Override
            public void log(Level level, String message) {
                if(level.equals(Level.INFO)){
                    assertTrue(Predicates.in(ImmutableList.<String>of("@off", "@fatal", "@error", "@warn", "@info")).
                            apply(message.toString()));
                }
                else if(level.equals(Level.DEBUG)){
                    assertTrue(Predicates.in(ImmutableList.<String>of("@off", "@fatal", "@error", "@warn", "@info", "@debug")).
                            apply(message));
                }
                else if(level.equals(Level.TRACE)){
                    assertTrue(Predicates.in(ImmutableList.<String>of("@off", "@fatal", "@error", "@warn", "@info", "@debug", "@trace")).
                            apply(message));
                }
                else if(level.equals(Level.ALL)){
                    assertTrue(Predicates.in(ImmutableList.<String>of("@off", "@fatal", "@error", "@warn", "@info", "@debug", "@trace", "@all")).
                            apply(message));
                }
            }
        });

        LoggerSettings.setLogLevel(name, Level.INFO);

        Logger logger = LoggerSettings.getInstance(name);
        System.out.println("LEVEL="+logger.getLevel());
        logger.fatal("@fatal");
        logger.error("@error");
        logger.warn("@warn");
        logger.info("@info");
        logger.debug("@debug");
        logger.trace("@trace");

        /*
        LoggerSettings.setLogLevel(name, Level.DEBUG);

        logger = LoggerSettings.getInstance(name);
        logger.fatal("@fatal");
        logger.error("@error");
        logger.warn("@warn");
        logger.info("@info");
        logger.debug("@debug");
        logger.trace("@trace");


        LoggerSettings.setLogLevel(name, Level.TRACE);

        logger = LoggerSettings.getInstance(name);
        logger.fatal("@fatal");
        logger.error("@error");
        logger.warn("@warn");
        logger.info("@info");
        logger.debug("@debug");
        logger.trace("@trace");
        */
    }

    @Test
    public void testGetInstance() throws Exception {
        //todo
    }

    @Test
    public void testGetValidLoggerName() throws Exception {
        //todo
    }
}
